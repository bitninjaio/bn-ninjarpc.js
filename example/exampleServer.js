const NinjaRpcServer = require('../index').NinjaRpcServer;

const server = new NinjaRpcServer({
  hostname: process.env.RABBIT_SERVER || '127.0.0.1',
  username: 'guest',
  password: 'guest',
  port: 5672,
  vhost: '/',
});

const endPoints = {
  create: create,
  get: get,
  update: update,
};

const events = {
  ping: ping,
};

async function create(params) {
  console.log(`Test service [CREATE]. Params: ${params}`);
  return {
    status: 'ok',
    method: 'create',
  };
}

async function get(params) {
  console.log(`Test service [GET]. Params:`, params);
  return {
    status: 'ok',
    method: 'get',
  };
}

async function update(params) {
  console.log(`Test service [UPDATE]. Params: ${params}`);
  return {
    status: 'ok',
    method: 'update',
  };
}

async function ping(params) {
  console.log(`Test service [PING]. Params: `, params);
}

try {
  server.listen('TEST_SERVICE', endPoints);
  server.subscribe('TEST_SERVICE', events);
} catch (e) {
  console.error(e);
}
