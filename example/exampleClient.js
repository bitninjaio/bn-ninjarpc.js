const NinjaRpcClient = require('../index').NinjaRpcClient;

const client = new NinjaRpcClient({
  hostname: process.env.RABBIT_SERVER || '127.0.0.1',
  username: 'guest',
  password: 'guest',
  port: 5672,
  vhost: '/',
});

setInterval(async () => {
  const create = await client.call('TEST_SERVICE', 'create', {
    test: 'test',
    anotherParam: 'moreTest',
  });
  console.log(create);

  const get = await client.call('TEST_SERVICE', 'get', {
    test: 'myTest',
    anotherParam: 'moreTest',
  });
  console.log(get);

  const update = await client.call('TEST_SERVICE', 'update', {
    test: 'asdasd',
    anotherParam: 'asdsa',
  });
  console.log(update);
}, 2000);

setInterval(async () => {
  client.emit('TEST_SERVICE', 'ping', {
    param1: 'val1',
    param2: 'val2',
  });
  console.log('Ping');
}, 1000);
