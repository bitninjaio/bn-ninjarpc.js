# BitNinjaIO NinjaRPC

RabbitMQ rpc server and client by BitNinja.io based on [PHP implementation](https://packagist.org/packages/bitninja/ninjarpc)

## Installation

Using npm:

```
$ npm i @bitninja/ninjarpc
```

## Usage

The package contains a client and a server as well, which can communicated each other.

### Server

```js
// Load the package
const NinjaRpcServer = require("@bitninja/ninjarpc").NinjaRpcServer;

// Create a new server
const server = new NinjaRpcServer({
  hostname: "127.0.0.1",
  username: "guest",
  password: "guest",
  port: 5672,
  vhost: "/"
});

// List of endpoints, keys are the endpoints
// values are callback functions
const endpoints = {
  endpoint1: endpoint1
};

// Example endpoint
function endpoint1(params) {
  console.log(`Endpoint 1 was called with ${params} parameters.`);
}

// Server listens on the given endpoints
server.listen("TEST_SERVICE", endpoints);
```

### Client

```js
// Load the client package
const NinjaRpcClient = require("@bitninja/ninjarpc").NinjaRpcClient;

// Create a client
const client = new NinjaRpcClient({
  hostname: "127.0.0.1",
  username: "guest",
  password: "guest",
  port: 5672,
  vhost: "/"
});

// Simply just call an endpoint
client.call("TEST_SERVICE", "endpoint1", { customattr: "customValue" });
```
