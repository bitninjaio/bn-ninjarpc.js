jest.mock('amqplib', () => require('amqplib-mocks'));

describe('RPC one client one server', () => {
  let client;
  let server;

  /**
   * Create stateful client / server pair before every test.
   */
  beforeEach(() => {
    const NinjaRpcClient = require('../../lib/rpcClient');
    const NinjaRpcServer = require('../../lib/rpcServer');

    const options = {
      hostname: 'stub-server',
      username: 'guest',
      password: 'guest',
      port: 5672,
      vhost: '/',
    };

    client = new NinjaRpcClient(options, null, `test-client-1`);
    server = new NinjaRpcServer(options, null, `test-server-1`);
  });

  /**
   * Close connections when the tes finished, to lose state.
   */
  afterEach(async () => {
    return Promise.all([client.disconnect(), server.disconnect()]);
  });

  it.skip('should execute a sum RPC call', async () => {
    expect.assertions(1);

    server.listen('case01', {
      sum(params) {
        return params.a + params.b;
      },
    });

    const result = client.call('case01', 'sum', {
      a: 5,
      b: 8,
    });

    return result.then((r) => {
      expect(r.result).toBe(13);
    });
  }, 1000);

  it.skip('should handle multiple service name', async () => {
    expect.assertions(4);

    server.listen('service1', {
      sum(params) {
        return params[0] + params[1];
      },
    });

    server.listen('service2', {
      divice(params) {
        return params[0] / params[1];
      },
    });

    const calls = [
      client.call('service2', 'divice', [50, 10]),
      client.call('service1', 'sum', [10, 5]),
      client.call('service2', 'divice', [20, 4]),
      client.call('service1', 'sum', [10, 6]),
    ];

    return Promise.all(calls).then((values) => {
      expect(values[0].result).toBe(5);
      expect(values[1].result).toBe(15);
      expect(values[2].result).toBe(5);
      expect(values[3].result).toBe(16);
    });
  }, 1000);

  it.skip('should handle multiple message to the same consumer', async () => {
    expect.assertions(100);

    server.listen('service3', {
      sum(params) {
        return {
          total: params[0] + params[1],
          from: params,
        };
      },
    });

    const calls = [];

    for (let x = 0; x < 100; x++) {
      calls.push(client.call('service3', 'sum', [x, 42]));
    }

    return Promise.all(calls).then((values) => {
      for (const response of values) {
        expect(response.result.total).toBe(
          response.result.from[0] + response.result.from[1],
        );
      }
    });
  }, 2000);

  it.skip('should receive the ready message', async () => {
    return new Promise((ok) => {
      server.subscribe('case02', {
        ready(params) {
          expect(params).toBe('hello');

          ok();
        },
      });

      setTimeout(() => client.emit('case02', 'ready', 'hello1'), 100);
    });
  }, 1000);
});
