const connectionOptions = {
  hostname: 'localhost',
  username: 'guest',
  password: 'guest',
  port: 5672,
  vhost: '/',
};

jest.mock('amqplib', () => {
  const mocker = require('amqplib-mocks');

  return {
    connect(url) {
      console.log(`Connecting the MOCK`, url);

      return mocker.connect(url);
    },
  };
});

const Client = require('../../lib/rpcClient');

describe('Connection management', () => {
  /**
   * todo: For connection we use AmqpConnectionManager instead of amqplib.
   *  For this package there is no mock package. Pls fix this test.
   */
  it.skip('should connect with the client', async () => {
    //expect.assertions(1);

    const client = new Client(connectionOptions, null, `try-connect`);

    setTimeout(() => {
      client.emit(`test`, 1);
    }, 100);

    return client.connect().then((url) => {
      expect(url).toBe(`amqp://guest:guest@localhost:5672/?heartbeat=5`);
    });
  }, 2200);
});
