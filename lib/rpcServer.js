const RabbitMQService = require('./queueManagers/RabbitMQ');
const Logger = require('./helpers/logger');
const UUID = require('uuid');

class NinjaRpcServer {
  /**
   * Creates an instance of NinjaRpcClient.
   *
   * @param {ConnectionManager.AmqpConnectionManagerOptions} options
   * @param {RabbitMQService} [queueManager=null]
   * @param {string} serverId Unique ID for the server, this is used in place for identifing.
   * @memberof NinjaRpcClient
   */
  constructor(options, queueManager = null, serverId) {
    /**
     * @type {string}
     */
    this.serverId = serverId || UUID.v4().substr(0, 8);

    this.logger = Logger(`NinjaRPC/Server/${this.serverId}`);

    this.queueManager =
      queueManager || new RabbitMQService(options, this.logger);

    process.on('SIGTERM', () => {
      this.disconnect();
    });

    process.on('SIGINT', () => {
      this.disconnect();
    });
  }

  /**
   *
   */
  async disconnect() {
    return this.queueManager.disconnect();
  }

  /**
   *
   */
  async connect() {}

  /**
   *
   * @param {String} serviceName
   * @param {*} routes
   */
  async listen(serviceName, routes) {
    this.queueManager.listen(serviceName, routes);
  }

  async subscribe(serviceName, routes) {
    this.queueManager.subscribe(serviceName, routes);
  }
}

module.exports = NinjaRpcServer;
