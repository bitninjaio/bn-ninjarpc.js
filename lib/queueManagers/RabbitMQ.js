const ConnectionManager = require('amqp-connection-manager');
const Command = require('../command');
const JSONSerializer = require('../serializers/json.serializer');

class RabbitMQService {
  /**
   * Creates an instance of RabbitMQService.
   *
   * @param {ConnectionManager.AmqpConnectionManagerOptions} options
   * @param {*} logger Instance of the main logger.
   * @memberof RabbitMQService
   */
  constructor(options, logger) {
    this.logger = logger.child({
      owner: `QueueManager`,
    });

    /**
     * @type {ConnectionManager.AmqpConnectionManagerOptions}
     */
    this.options = options;
    this.connection = null;
    this.channelWrapper = null;
    this.failedAttempt = 0;
    this.queueTimeout = 60000;

    // Initiate the shared serializer.
    this.serialzer = new JSONSerializer();

    // Track the list of queues added to the channel setup call.
    this.openQueueList = [];
  }

  /**
   * Close the connection and release the resources.
   *
   * @returns {Promise<void>}
   * @memberof RabbitMQService
   */
  async disconnect() {
    try {
      await this.connection.close();
    } catch (e) {
      // The connection manager throws a channel closed error when messages arrive without expectations.
      // We can ignore this.
      if (e.message === 'Channel closed') {
        return;
      } else {
        //throw e;
      }
    }
  }

  /**
   * Build a connection string to the RabbitMQ server from the provided options.
   *
   * @param {*} options
   * @returns {string}
   * @memberof RabbitMQService
   */
  getDSN(options) {
    return `amqp://${options.username}:${options.password}@${options.hostname}:${options.port}${options.vhost}`;
  }

  /**
   * Initializes the connection manager, internal use only!
   *
   * @returns {ConnectionManager.AmqpConnectionManager}
   * @memberof RabbitMQService
   */
  getConnectionManager() {
    if (!this.connection) {
      this.connection = ConnectionManager.connect(
        [this.getDSN(this.options)],
        {
          reconnectTimeInSeconds: 2,
          heartbeatIntervalInSeconds: 60
        },
      );

      this.handleConnectionEvents(this.connection);
    }

    return this.connection;
  }

  /**
   * Track connection / disconnection events and kill the container when the
   * maximum attempt reached.
   *
   * @param {ConnectionManager} connectionManager
   * @memberof RabbitMQService
   */
  handleConnectionEvents(connectionManager) {
    connectionManager.on('connect', () => {
      this.logger.info(`Connected`);
      this.failedAttempt = 0;
    });

    connectionManager.on('disconnect', (event) => {
      if (event.err.code) {
        this.logger.error(`Forceful disconnect`, event.err);
      } else {
        this.logger.info(`Graceful disconnect`);
      }

      if (event.err.code && event.err.code === 'ECONNREFUSED') {
        this.failedAttempt++;
        this.logger.debug(`Failed attempts: ${this.failedAttempt}`);
      }

      if (this.failedAttempt > 4) {
        this.logger.error(
          `Couldn't connect to RabbitMQ 5 times. Application shutdown...`,
        );
        process.exit(1);
      }
    });
  }

  /**
   * Initializes the channel manager with the shared connection manager.
   *
   * @returns {ConnectionManager.ChannelWrapper}
   * @memberof RabbitMQService
   */
  getChannelWrapper() {
    if (!this.channelWrapper) {
      this.channelWrapper = this.getConnectionManager().createChannel({
        json: false,
        confirm: false
      });

      this.handleChannelEvents(this.channelWrapper);
    }

    return this.channelWrapper;
  }

  /**
   * Log the channel events.
   *
   * @param {ConnectionManager.ChannelWrapper} channelWrapper
   * @memberof RabbitMQService
   */
  handleChannelEvents(channelWrapper) {
    channelWrapper.on('connect', () => this.logger.info(`Channel established`));
    channelWrapper.on('close', () => this.logger.error(`Channel closed`));
    channelWrapper.on('error', (err) =>
      this.logger.error(`RabbitMQ channel error!`, err),
    );
  }

  /**
   * Add setup to channel wrapper
   * @param {function} setup
   * @return {Promise<void>}
   */
  async addSetup(setup) {
    await this.getChannelWrapper().addSetup(setup);
  }

  /**
   * Remove setup to channel wrapper
   * @param {function} setup
   * @return {Promise<void>}
   */
  async removeSetup(setup) {
    await this.getChannelWrapper().removeSetup(setup, () => {
    });
  }

  /**
   * Creates a queue
   *
   * @param {string} queueName
   * @param {number|null} expires
   * @param {boolean} saveSetup
   */
  async assertQueue(queueName, expires = null, saveSetup = true) {
    const setup = (channel) => {
      this.logger.info(`Queue [${queueName}] is added to the base setup.`);

      if (!saveSetup) {
        this.removeSetup(setup);
      }

      return channel.assertQueue(queueName, {
        exclusive: false,
        autoDelete: true,
        durable: false,
        expires,
      });
    };

    if (!saveSetup) {
      await this.addSetup(setup);
      return;
    }

    // If saveSetup is true, then save to queueName too.
    if (!this.openQueueList.includes(queueName)) {
      this.openQueueList.push(queueName);
      await this.addSetup(setup);
    } else {
      this.logger.debug(`Queue [${queueName}] already initialized.`);
    }
  }

  /**
   * Subscribe a handler for the given RPC command.
   *
   * @param {string} queueName
   * @param {() => Command} handler
   */
  async respondToCommand(queueName, handler) {
    const setup = (channel) => {
      this.logger.debug(
        `Initializng responder for [${queueName}] RPC commands`,
      );

      channel.consume(
        queueName,
        async (message) => {
          // Deserialize the command from the binary format.
          const command = this.serialzer.unserialize(message.content);
          // Process the handler and load it back to the command ~
          command.result = await handler(command.params);


          // Legacy queue handler with queue timeout.
          await this.assertQueue(command.responseQueueName, this.queueTimeout, false);
          this.publish(command.responseQueueName, command);
        },
        {
          noAck: true,
        },
      );
    };
    this.addSetup(setup);
  }

  /**
   * Consume queue messages and pass it to the callback
   *
   * @param {string} queueName
   * @param {*} handler
   */
  async __subscribe(queueName, handler) {
    return new Promise(async (resolve, reject) => {
      const setup = (channel) => {
        channel.consume(
          queueName,
          (msg) => {
            if (!msg) {
              reject(`Queue does not exists.`);
            } else {
              handler(this.serialzer.unserialize(msg.content));
            }
          },
          {
            noAck: true,
          }
        );
      }

      await this.assertQueue(queueName, this.queueTimeout);
      this.addSetup(setup);
    });

  }

  async consumeEvent(queueName, callback) {
    const setup = (channel) => {
      channel.consume(
        queueName,
        async (request) => {
          if (request !== null) {
            const response = this.serialzer.unserialize(request.content);

            callback(response.params);
          } else {
            this.logger.warn(`Gor an empty request in [${queueName}]`);
          }
        },
        {
          noAck: true,
        },
      );
    };

    this.addSetup(setup);
  }

  /**
   * Publish message to a queue
   *
   * @param {*} queueName
   * @param {*} message
   * @param {*} exchange
   */
  async publish(queueName, message, exchange = '') {
    return this.getChannelWrapper().publish(
      exchange,
      queueName,
      this.serialzer.serialize(message),
    );
  }

  /**
   * Delete a queue
   *
   * @param {*} queueName
   */
  async deleteQueue(queueName) {
    const setup = async (channel) => {
      this.removeSetup(setup);
      return await channel.deleteQueue(queueName);
    }

    this.addSetup(setup);
  }

  /**
   * Serve command handlers for RPC calls.
   *
   * @memberof RabbitMQService
   */
  async listen(service, routes) {
    this.logger.info('Starting NinjaRPC [RabbitMQ] server...');

    for (let path of Object.keys(routes)) {
      const queueName = `${service}_${path}`;
      this.logger.info(`Waiting for messages in [${queueName}]`);

      await this.assertQueue(queueName);
      this.respondToCommand(queueName, routes[path]);
    }
  }

  /**
   * Subscribe for event queues.
   *
   * @param {*} service
   * @param {*} routes
   * @memberof RabbitMQService
   */
  async subscribe(service, routes) {
    this.logger.info('Subscribing for event handlers', service);

    for (let path of Object.keys(routes)) {
      const queueName = `${service}_${path}`;
      this.logger.info(`Waiting for messages in [${queueName}]`);

      await this.assertQueue(queueName);
      this.consumeEvent(queueName, routes[path]);
    }
  }
}

module.exports = RabbitMQService;
