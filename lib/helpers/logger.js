const pino = require('pino');

/**
 * Create a configured logger instance.
 *
 * @param {string} name Used to associate the class / module.
 * @returns {pino.Logger}
 */
function Logger(name) {
  const isDevelopment =
    process.env.NODE_ENV == false || process.env.NODE_ENV === 'development';

  const isTesting = process.env.NODE_ENV === 'testing';

  const instance = pino({
    name,
    level: isTesting ? 'silent' : isDevelopment ? 'debug' : 'info',
    useLevelLabels: true,
    prettyPrint: isDevelopment,
  });

  instance.debug('Logger initialized');

  return instance;
}

module.exports = Logger;
