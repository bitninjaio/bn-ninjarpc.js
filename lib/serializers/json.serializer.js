const Command = require('../command');

class JSONSerializer {
  /**
   * Build a JSON serialized buffer from a message.
   *
   * @param {Command} message
   * @returns {Buffer}
   * @memberof JSONSerializer
   */
  serialize(message) {
    return Buffer.from(JSON.stringify(message));
  }

  /**
   * Parse the JSON serialized buffer into javascript object.
   *
   * @param {Buffer} buffer
   * @returns {Command}
   * @memberof JSONSerializer
   */
  unserialize(buffer) {
    return JSON.parse(buffer.toString());
  }
}

module.exports = JSONSerializer;
