const {EventEmitter} = require('events');

class ReplyRouter extends EventEmitter {
  constructor(queueName, queueManager) {
    super();

    this.queueManager = queueManager;
    this.fastReplyQueue = queueName;

    this.queueManager.__subscribe(this.fastReplyQueue, (reply) => {
      this.emit(reply.correlationId, reply);
    });
  }

  async register(message) {
    return await new Promise((arrived) => {
      this.once(message.correlationId, arrived);

      setTimeout(() => {
        this.removeListener(message.correlationId, arrived);
      }, this.queueManager.options.queueTimeout);
    });
  }
}

module.exports = ReplyRouter;
