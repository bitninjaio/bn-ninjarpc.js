const RabbitMQService = require('./queueManagers/RabbitMQ');
const ConnectionManager = require('amqp-connection-manager');
const Logger = require('./helpers/logger');
const Command = require('./command');
const ReplyRouter = require('./reply-router');
const UUID = require('uuid');

class NinjaRpcClient {
  /**
   * Creates an instance of NinjaRpcClient.
   *
   * @param {ConnectionManager.AmqpConnectionManagerOptions} options
   * @param {RabbitMQService} [queueManager=null]
   * @param {string} clientId Unique ID for the client, this is used for reply routing.
   * @memberof NinjaRpcClient
   */
  constructor(options, queueManager = null, clientId) {

    if(options.queueTimeout === undefined){
      options.queueTimeout = 60000;
    }

    if(options.responseQueueName === undefined){
      options.responseQueueName = 'faster-reply';
    }

    /**
     * @type {string}
     */
    this.clientId = clientId || UUID.v4().substr(0, 8);
    this.logger = Logger(`NinjaRPC/Client/${this.clientId}`);
    const responseQueueName = 'RPC_CLIENT.' + options.responseQueueName + '.' + this.clientId;

    this.queueManager =
      queueManager || new RabbitMQService(options, this.logger);

    this.replyRouter = new ReplyRouter(responseQueueName, this.queueManager);

    process.on('SIGTERM', () => {
      this.disconnect();
    });

    process.on('SIGINT', () => {
      this.disconnect();
    });
  }

  connect() {
    const connectionManager = this.queueManager.getConnectionManager();

    // Already connected, resolve instantly.
    if (connectionManager.isConnected()) {
      return Promise.resolve(connectionManager);
    }

    this.logger.warn('Creating the promise', {
      isConnected: connectionManager.isConnected(),
    });

    return new Promise((resolve) => {
      this.logger.warn('Promise called');

      connectionManager.once('connect', () => {
        this.logger.error(`Promise solved`);
        resolve();
      });
    });
  }

  async disconnect() {
    return this.queueManager.disconnect();
  }

  async emit(serviceName, methodName, params, exchange = '') {
    const command = new Command({
      serviceName,
      methodName,
      params,
    });

    await this.queueManager.assertQueue(command.getSendingQueueName(), null, false);
    await this.queueManager.publish(command.getSendingQueueName(), command, exchange);
  }

  async call(serviceName, methodName, params, exchange = '') {
    this.logger.debug(`Calling`, serviceName, methodName, params);

    const command = new Command({
      serviceName,
      methodName,
      params,
      responseQueueName: this.replyRouter.fastReplyQueue,
    });
    const correlationId = command.correlationId;

    this.logger.debug(`Creating RPC command`, command);

    return new Promise(async (resolve, reject) => {
      const timeoutTimer = setTimeout(() => {
        this.logger.warn(`Command timed out!`, {correlationId});
        reject('ETIMEOUT');
      }, this.queueManager.options.queueTimeout);
      const clearTimer = () => timeoutTimer && clearTimeout(timeoutTimer);
      const resultPromise = this.replyRouter.register(command);

      await this.queueManager.assertQueue(command.getSendingQueueName(), null, false);
      await this.queueManager.publish(command.getSendingQueueName(), command, exchange);

      resultPromise
        .then((response) => {
          clearTimer();
          this.logger.debug(`Command arrived`, {correlationId});

          if (
            typeof response === 'object' &&
            typeof response.serviceName === 'string' &&
            typeof response.methodName === 'string'
          ) {
            resolve(response);
          } else {
            this.logger.error(`Command has bad response format`, {
              correlationId,
              response,
            });

            reject('EBADRESPONSE');
          }
        })
        .catch((error) => {
          clearTimer();

          this.logger.warn(`Command error`, {
            correlationId,
            context: error,
          });

          reject(error);
        })
    }).catch((reason) => {
      throw new Error(`RPC client call exception! Server queue: [${command.getSendingQueueName()}], reason: [${reason}]!`);
    });
  }
}

module.exports = NinjaRpcClient;
