/**
 * Response object for NinjaRPC response
 * @param int status
 * @param string|null result
 * @param string|null message
 * @param object errors
 * @returns {{result: *, success: boolean, message: string|null, errors: object}}
 */
module.exports = (status = 200, result = null, message = null, errors = {}) => {
  return {
    success: status >= 100 && status < 400 ? true : false,
    status,
    result,
    message,
    errors,
  };
};
