const uuid = require('uuid');

class Command {
  constructor(args) {
    /**
     * @type {string}
     */
    this.correlationId = args.correlationId || uuid.v4();

    /**
     * @type {string}
     */
    this.serviceName = args.serviceName;

    /**
     * @type {string}
     */
    this.methodName = args.methodName;

    /**
     * @type {*}
     */
    this.params = args.params;

    /**
     * @type {string}
     */
    this.responseQueueName =
      args.responseQueueName ||
      this.getSendingQueueName() + `_` + this.correlationId;

    /**
     * @type {string|null}
     */
    this.traceId = args.traceId || null;

    /**
     * @type {string|null}
     */
    this.spanId = args.spanId || null;

    /**
     * @type {*|null}
     */
    this.result = args.result || null;

    /**
     * @type {string|null}
     */
    this.parnetId = args.parnetId || null;

    /**
     * @type {boolean|null}
     */
    this.sampled = args.sampled || null;

    /**
     * @type {*|null}
     */
    this.flags = args.flags || null;
  }

  /**
   * Build the sending queue name from the service name and method name.
   *
   * @returns {string}
   * @memberof Command
   */
  getSendingQueueName() {
    return this.serviceName + '_' + this.methodName;
  }
}

module.exports = Command;
